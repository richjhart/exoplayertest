package com.rjhartsoftware.videotest;

import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ClippingMediaSource;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaPeriod;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.SingleSampleMediaSource;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.ads.AdPlaybackState;
import com.google.android.exoplayer2.source.ads.AdsLoader;
import com.google.android.exoplayer2.source.ads.AdsMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        copyAssets();

        // 1. Create a default TrackSelector
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        // 2. Create the player
        SimpleExoPlayer player =
                ExoPlayerFactory.newSimpleInstance(this, trackSelector);

        PlayerView playerView = findViewById(R.id.main_player);
        playerView.setPlayer(player);

        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, "yourApplicationName"));
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new HlsMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse("http://184.72.239.149/vod/smil:BigBuckBunny.smil/playlist.m3u8"));

        AdsMediaSource adSource = new AdsMediaSource(videoSource, mAdMediaSourceFactory, mAdsLoader, null, null, null);

        player.setPlayWhenReady(true);
        // Prepare the player with the source.
        player.prepare(adSource);
    }

    private final AdsMediaSource.MediaSourceFactory mAdMediaSourceFactory = new AdsMediaSource.MediaSourceFactory() {
        @Override
        public MediaSource createMediaSource(Uri uri, @Nullable Handler handler, @Nullable MediaSourceEventListener listener) {
            DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(MainActivity.this,
                    Util.getUserAgent(MainActivity.this, getString(R.string.app_name)));

            return new ExtractorMediaSource.Factory(dataSourceFactory)
                    .createMediaSource(uri);
        }

        @Override
        public int[] getSupportedTypes() {
            return new int[] {C.TYPE_DASH, C.TYPE_HLS, C.TYPE_OTHER, C.TYPE_SS};
        }
    };

    private final MyAdsLoader mAdsLoader = new MyAdsLoader();

    private final class MyAdsLoader implements AdsLoader {

        @Override
        public void setSupportedContentTypes(int... contentTypes) {

        }

        @Override
        public void attachPlayer(ExoPlayer player, EventListener eventListener, ViewGroup adUiViewGroup) {
            AdPlaybackState ads = new AdPlaybackState(120L*1000000L,
                    240L*1000000L,
                    360L*1000000L,
                    480L*1000000L);
            ads.adGroups[0] = ads.adGroups[0].withAdCount(1);
            ads.adGroups[0] = ads.adGroups[0].withAdUri(Uri.fromFile(mFilenames[0]), 0);
            ads.adGroups[1] = ads.adGroups[1].withAdCount(1);
            ads.adGroups[1] = ads.adGroups[1].withAdUri(Uri.fromFile(mFilenames[1]), 0);
            ads.adGroups[2] = ads.adGroups[2].withAdCount(1);
            ads.adGroups[2] = ads.adGroups[2].withAdUri(Uri.fromFile(mFilenames[2]), 0);
            ads.adGroups[3] = ads.adGroups[3].withAdCount(1);
            ads.adGroups[3] = ads.adGroups[3].withAdUri(Uri.fromFile(mFilenames[3]), 0);
            eventListener.onAdPlaybackState(ads);
        }

        @Override
        public void detachPlayer() {

        }

        @Override
        public void release() {

        }

        @Override
        public void handlePrepareError(int adGroupIndex, int adIndexInAdGroup, IOException exception) {

        }
    }

    private File[] mFilenames = new File[4];

    private void copyAssets() {
        AssetManager assetManager = getAssets();
        for(int i = 0; i < 4; i++) {
            InputStream in;
            OutputStream out;
            try {
                in = assetManager.open((i + 1) + ".mp4");
                File outFile = new File(getFilesDir(), (i + 1) + ".mp4");
                out = new FileOutputStream(outFile);
                copyFile(in, out);
                in.close();
                out.flush();
                out.close();
                mFilenames[i] = outFile;
            } catch(IOException e) {
                Log.e("tag", "Failed to copy asset file: " + i, e);
            }
        }
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }
}
